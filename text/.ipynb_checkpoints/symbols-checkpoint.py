# """ from https://github.com/keithito/tacotron """

'''
Defines the set of symbols used in text input to the model.
'''

_pad = '_'
_eos = '~'
_characters = 'abcdefghijklmnopqrstuvwxyzđáàãảạăắằẵẳặâấầẫẩậóòõỏọôốồỗổộơớờỡởợéèẽẻẹêếềễểệúùũủụưứừữửựíìĩỉịýỳỹỷỵ!(),-.:;? '

# Export all symbols:
symbols = [_pad, _eos] + list(_characters)
