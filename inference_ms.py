import matplotlib.pyplot as plt

import os
import json
import math
import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader

import commons
import utils
from data_utils import TextAudioLoader, TextAudioCollate, TextAudioSpeakerLoader, TextAudioSpeakerCollate
from models import SynthesizerTrn
from text.symbols import symbols
from text import text_to_sequence

from scipy.io.wavfile import write

# device = torch.device("cpu")
# model.to(device)
def get_text(text, hps):
    text_norm = text_to_sequence(text.lower(), hps.data.text_cleaners)
    print("====", text_norm)
    if hps.data.add_blank:
        text_norm = commons.intersperse(text_norm, 0)
    print("++++", text_norm)
    text_norm = torch.LongTensor(text_norm)
    return text_norm


hps = utils.get_hparams_from_file("./configs/vlsp_base.json")

net_g = SynthesizerTrn(
    len(symbols),
    hps.data.filter_length // 2 + 1,
    hps.train.segment_size // hps.data.hop_length,
    n_speakers=hps.data.n_speakers,
    **hps.model).cuda()
_ = net_g.eval()

_ = utils.load_checkpoint("./logs/vlsp_base/G_42000.pth", net_g, None)

stn_tst = get_text("anh đừng có làm trò con bò", hps)
import time
start = time.time()
with torch.no_grad():
    x_tst = stn_tst.cuda().unsqueeze(0)
    x_tst_lengths = torch.LongTensor([stn_tst.size(0)]).cuda()
    sid = torch.LongTensor([0]).cuda()
    audio = net_g.infer(x_tst, x_tst_lengths, sid=sid, noise_scale=.667, noise_scale_w=0.8, length_scale=1)[0][0,0].data.cpu().float().numpy()

print("TIME: ", time.time()-start)    
from  scipy.io import wavfile 
sampling_rate = 22050
import soundfile as sf
sf.write('demo.wav', audio, sampling_rate)
# ipd.display(ipd.Audio(audio, rate=hps.data.sampling_rate, normalize=False))