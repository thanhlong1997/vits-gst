# VITS GST: Conditional Variational Autoencoder with Adversarial Learning for End-to-End Text-to-Speech



Emotional speech synthesis is a challeng-
ing task in speech processing. To build an
emotional TTS system, one would need to
have a quality emotional dataset of the target
speaker. However, collecting such dataset is dif-
ficult, sometimes even impossible. This paper
presents our approach that address the problem
of transplanting a source speaker’s emotional
expression to a target speaker, one of the VLSP
2022 TTS tasks.

## Pre-requisites
0. Python >= 3.6
0. Clone this repository
0. Install python requirements. Please refer [requirements.txt](requirements.txt)
    1. You may need to install espeak first: `apt-get install espeak`
0. Build Monotonic Alignment Search and run preprocessing if you use your own datasets.
```sh
# Cython-version Monotonoic Alignment Search
activate your virtual environment
cd monotonic_align
python setup.py build_ext --inplace

```

## Data
Each line in filelists/task2_train.txt.cleaned and filelists/task2_val.txt.cleaned is delimited by | into 3 fields
* file path
* emotion id
* text content
for example: 
path_to_audio/16199.wav|0|chúng ta đã cố thủ trong nhà an dưỡng
### Specification
* Audio File Type: wav
* Sample Rate: 22050 KHZ
* Number of train Audio Files task 2: 9,527
* Number of train Audio Files task 2: 229

data task 1 for baseline model training download [here](https://drive.google.com/drive/folders/1qmB-Ir1V_YprbNaT1aEo7DrMz6gKynxJ?usp=sharing)\
Training data task 2 download [here](https://drive.google.com/drive/folders/1eJ7RoIWmaQmMGHmUuZlbzdF3PI4rTJ00?usp=sharing)\
Eval data task 2 download [here](https://drive.google.com/drive/folders/1xVn6MWdkDp1We3ZYzsFkiPEny6QqqQxV?usp=sharing) 

## Training Example
0. Training baseline model and copy the latest model to logs/pre_trained
```sh
   sh VLSP_task2_base_model.sh
```
or download our pre-trained model [here](https://drive.google.com/file/d/1hqdZ3ONg5YjJob3cGx7dnAZ7AqY2CeBZ/view?usp=sharing)  and copy to logs/pre_trained
1. Training vlsp taks 2
```sh
   sh VLSP_task2_training.sh
```
## Inference Example
See [inference.ipynb](inference.ipynb)
