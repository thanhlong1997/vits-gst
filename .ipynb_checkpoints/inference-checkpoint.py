from nltk import sent_tokenize
import re
import string
import nltk

nltk.download('punkt')

# for the first run time, run following commands:
# "import nltk"
# "nltk.download('punkt')"


DELIMITERS = [',', ':', '"', ';']
VIETNAMESE_CHARACTERS = list(
    'abcdefghijklmnopqrstuvwxyzđáàãảạăắằẵẳặâấầẫẩậóòõỏọôốồỗổộơớờỡởợéèẽẻẹêếềễểệúùũủụưứừữửựíìĩỉịýỳỹỷỵ')


def split(text_list, max_length=100):
    if len(text_list) == 0 or (len(text_list) == 1 and len(
            [e for e in re.split('|'.join(DELIMITERS), text_list[0]) if e.strip() != '']) <= 1):
        return text_list

    result = []
    index = 0
    while index < len(text_list):
        temp = ''
        while index < len(text_list) and len(temp + text_list[index]) < max_length:
            temp += text_list[index].strip() + ' '
            index += 1
        if temp != '':
            result.append(temp.strip())
        else:
            if len(text_list[index]) >= max_length:
                test = text_list[index]
                for d in DELIMITERS:
                    test = re.sub(d, d + '@', test)
                result.extend([e.strip() for e in split([f.strip() for f in re.split('@', test) if f.strip() != ''],
                                                        max_length=max_length)])
            index += 1

    return result


def split_text(text, max_length=100):
    text = re.sub(chr(8230), '..', text)  # replace '...' by '.'
    text = re.sub(r'[\n]+', '. ', text)
    sent_list = sent_tokenize(text)
    sent_list = [e for e in sent_list if len([i for i in list(e.lower()) if i in VIETNAMESE_CHARACTERS]) > 0]
    if len(sent_list) == 0:
        return [text]
    texts = split(sent_list, max_length)
    new_texts = []
    for _text in texts:
        if len(text) < 4:
            _text += '.'
        if _text.strip()[-1] in string.punctuation and _text.strip()[-1] != '>':
            _text = _text.rstrip(_text.strip()[-1])
            _text += '.'
        if _text.strip()[-1] not in string.punctuation:
            _text += '.'
        new_texts.append(_text)

    return new_texts


def try_cast_to(_input, dtype=float, default=0.0):
    try:
        _output = dtype(_input)
    except Exception as e:
        print("Error: ", str(e))
        print("convert {} sang kieu {} khong thanh cong! su dung gia tri default".format(_input, dtype.__name__))
        _output = dtype(default)
    return _output


def replace_specical_characters(text):
    from six.moves.html_parser import HTMLParser
    h = HTMLParser()
    text = h.unescape(text)
    return text


class TextSpeech_Processing(object):
    def __init__(self):
        # params
        self.max_length_text = 2000
        self.text_split = False
        self.auto_silence = False

    def _split_text(self, text, max_length_text=200, text_split=False, auto_silence=False):
        max_length_text = self.max_length_text
        text = replace_specical_characters(text)
        texts = self._split_text_by_tag(text)
        new_texts = []
        for text in texts:
            if text.startswith("<br"):
                new_texts.append(text)
            else:
                text = self._replace_punct(text, text_split, auto_silence)
                _texts = self._split_text_by_tag(text)
                for __text in _texts:
                    new_texts.extend(split_text(__text, max_length_text))
        return new_texts

    def _split_text_by_tag(self, text):
        tags = re.finditer(r"<.*?>", text)
        texts = []
        start = 0
        for tag in tags:
            if tag.start() == 0:
                texts.append(tag.group())
                start = tag.end()
            elif start - tag.start() == 0:
                texts.append(tag.group())
                start = tag.end()
                continue
            else:
                texts.append(text[start:tag.start()])
                texts.append(tag.group())
                start = tag.end()
        texts.append(text[start:])
        return texts

    def _replace_punct(self, text, text_split=False, auto_silence=False):
        if not text_split and auto_silence:
            # text = re.sub(r"\.(?=(((?!\>).)*\<)|[^\<\>]*$)", ".<br 0.5>", text, flags=re.MULTILINE)
            # text = text.replace(", ", "<br 0.5>")
            text = text.replace("* ", "<br 0.1>")
            text = text.replace(". ", "<br 1.0>")
            text = text.replace("\n", "<br 2.0>")
        return text


import matplotlib.pyplot as plt

import os
import json
import math
import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader

import commons
import utils
from data_utils import TextAudioLoader, TextAudioCollate, TextAudioSpeakerLoader, TextAudioSpeakerCollate
from models import SynthesizerTrn
from text.symbols import symbols
from text import text_to_sequence

from scipy.io.wavfile import write

#####################################
import re
import unidecode

VIETNAMESE_CHARACTERS = list(
    'abcdefghijklmnopqrstuvwxyzđáàãảạăắằẵẳặâấầẫẩậóòõỏọôốồỗổộơớờỡởợéèẽẻẹêếềễểệúùũủụưứừữửựíìĩỉịýỳỹỷỵ')
_pad = '_'
_punctuation = '!\'(),.:;? '
_special = '-'
symbols = [_pad] + list(_special) + list(_punctuation) + VIETNAMESE_CHARACTERS


def sybol_normalize(text):
    text = re.sub(",\s*\.\.\.", ", ", text)

    while re.search("\.\.\.\s*\)", text):
        text = re.sub("\.\.\.\s*\)", ")", text)

    while re.search("\.\.\.", text):
        text = re.sub("\.\.\.", ".", text)

    while re.search("\n\n", text):
        text = re.sub("\n\n", "\n", text)
    while re.search(",\s*,", text):
        text = re.sub(",\s*,", ",", text)

    text = re.sub("\n", ". ", text)
    text = re.sub("\.\s*\.", ". ", text)
    text = re.sub(" \. ", ". ", text)
    text = re.sub(" , ", ", ", text)
    return text


def normalize_text(text):
    # print(text)
    text = text.strip("-")
    text = text.strip(" ")
    # text = text.rstrip('.')
    text = text.strip(",")
    text = re.sub("[:,;]\s", " , ", text)
    text = re.sub("[!?.]", " . ", text)
    text = re.sub("['\"–\\\]", " ", text)
    text = re.sub("[;]", " , ", text)
    text = re.sub("\t", " ", text)
    while re.search("  ", text):
        text = re.sub("  ", " ", text)
    text = sybol_normalize(text)
    text2 = ""
    for char in list(text):
        if not re.search("[a-z]", unidecode.unidecode(char.lower())):
            if unidecode.unidecode(char) != char:
                text2 += unidecode.unidecode(char)
            else:
                text2 += char
        else:
            text2 += char

    text = text2
    text = re.sub("[:,;]\s", " , ", text)
    while re.search("[a-zA-z]+\d+", text):
        group = re.search("[a-zA-z]+\d+", text).group()
        replace = (
                re.search("[a-zA-Z]+", group).group()
                + " "
                + re.search("\d+", group).group()
        )
        text = text.replace(group, replace)

    text = re.sub("-", " - ", text)
    text = re.sub('"', " ", text)
    text = re.sub("\)\s*,", ",", text)
    text = re.sub("\)\s*\.", ".", text)
    text = re.sub(",\(", ",", text)
    text = re.sub("\)", ",", text)
    text = re.sub("\(", ", ", text)
    text = re.sub("/", " ", text)
    text = re.sub(" - ", ", ", text)
    text = text.lower()

    while re.search("[^a-z*\s.,|\d]", unidecode.unidecode(text.lower())):
        replace = re.search("[^a-z*\s.,|\d]", unidecode.unidecode(text.lower()))
        start = replace.start()
        end = replace.end()
        text = text[:start] + " " + text[end:]

    list_sybole = list(symbols)
    list_sybole.extend(["|", "*"])
    text = "".join(x for x in text if x in list_sybole)
    while re.search("  ", text):
        text = re.sub("  ", " ", text)

    text = text.strip(" ")
    if len(text) > 0:
        while text[-1] in ["?", "!", " ", ","]:
            text = text[:-1] + "."
            if len(text) == 0:
                break
    text = sybol_normalize(text)
    text = text.strip(" ")
    return text


#####################################################


# device = torch.device("cpu")
# model.to(device)
def get_text(text, hps):
    text_norm = text_to_sequence(text.lower(), hps.data.text_cleaners)
    print("====", text_norm)
    if hps.data.add_blank:
        text_norm = commons.intersperse(text_norm, 0)
    print("++++", text_norm)
    text_norm = torch.LongTensor(text_norm)
    return text_norm


hps = utils.get_hparams_from_file("/home/jovyan/thangnv/vits/configs/vietnamese_base.json")

net_g = SynthesizerTrn(
    len(symbols),
    hps.data.filter_length // 2 + 1,
    hps.train.segment_size // hps.data.hop_length,
    **hps.model).cuda()
_ = net_g.eval()

_ = utils.load_checkpoint("/home/jovyan/thangnv/vits/logs/vietnamese_base/G_255000.pth", net_g, None)

# vi en pi ti sờ mát voi, Nền tảng giọng nói thông minh, được phát triển bởi đội ngũ chuyên gia và kỹ sư tài năng của tập đoàn vi en pi ti. Sản phẩm ứng dụng các công nghệ ây ai tiên tiến nhất, được tối ưu trên hạ tầng lớn, và được nuôi dưỡng bằng nguồn dữ liệu dồi dào. vi en pi ti sờ mát voi cung cấp ba giải pháp: Chuyển đổi văn bản thành giọng nói, chuyển đổi giọng nói thành văn bản và xác thực giọng nói. Giải pháp chuyển đổi văn bản thành giọng nói hỗ trợ chuyển đổi văn bản thành giọng nói nam, nữ ba miền Bắc, Trung, Nam với đa dạng phong cách từ đọc sách đến đọc báo, với ngữ điệu tự nhiên, được chuẩn hóa theo đặc trưng ngôn ngữ Tiếng Việt, cùng khả năng nhận dạng cũng như đọc các từ viết tắt. Giải pháp chuyển đổi giọng nói thành văn bản cho phép chuyển đổi từ luồng âm thanh trực tiếp hoặc từ phai âm thanh thành văn bản với tốc độ chuyển đổi nhanh và độ chính xác lên tới chín mươi tám phần trăm theo tập dữ liệu của khách hàng. Giải pháp xác thực giọng nói thông qua phương pháp sinh trắc học giọng nói hỗ trợ xác thực, nhận diện giọng nói của người dùng giúp tăng cường bảo mật trong các giao dịch trực tuyến và sử dụng thiết bị.
text = "vi en pi ti sờ mát voi, Nền tảng giọng nói thông minh, được phát triển bởi đội ngũ chuyên gia và kỹ sư tài năng của tập đoàn vi en pi ti. Sản phẩm ứng dụng các công nghệ ây ai tiên tiến nhất, được tối ưu trên hạ tầng lớn, và được nuôi dưỡng bằng nguồn dữ liệu dồi dào. vi en pi ti sờ mát voi cung cấp ba giải pháp: Chuyển đổi văn bản thành giọng nói, chuyển đổi giọng nói thành văn bản và xác thực giọng nói. Giải pháp chuyển đổi văn bản thành giọng nói, hỗ trợ chuyển đổi văn bản thành giọng nói nam nữ, ba miền Bắc Trung Nam, với đa dạng phong cách từ đọc sách đến đọc báo, với ngữ điệu tự nhiên, được chuẩn hóa theo đặc trưng ngôn ngữ Tiếng Việt, cùng khả năng nhận dạng cũng như đọc các từ viết tắt. Giải pháp chuyển đổi giọng nói thành văn bản cho phép chuyển đổi từ luồng âm thanh trực tiếp hoặc từ phai âm thanh thành văn bản với tốc độ chuyển đổi nhanh và độ chính xác lên tới chín mươi tám phần trăm theo tập dữ liệu của khách hàng. Giải pháp xác thực giọng nói thông qua phương pháp sinh trắc học giọng nói hỗ trợ xác thực, nhận diện giọng nói của người dùng giúp tăng cường bảo mật trong các giao dịch trực tuyến và sử dụng thiết bị."
process_text = TextSpeech_Processing()
splited_text = process_text._split_text(text)
import time

start = time.time()
wavs = []
for txt in splited_text:
    txt = normalize_text(txt)
    print(txt)
    stn_tst = get_text(txt, hps)

    with torch.no_grad():
        x_tst = stn_tst.cuda().unsqueeze(0)
        x_tst_lengths = torch.LongTensor([stn_tst.size(0)]).cuda()

        # x_tst = stn_tst.cpu().unsqueeze(0)
        # x_tst_lengths = torch.LongTensor([stn_tst.size(0)]).cpu()

        audio = net_g.infer(x_tst, x_tst_lengths, noise_scale=.667, noise_scale_w=0.8, length_scale=1)[0][
            0, 0].data.cpu().float().numpy()
    wavs.append(audio)

import numpy as np
import librosa

silent_duration = 0.2
silent_wav = np.zeros((int(silent_duration * 22050)))
new_wavs = []
final_wav = wavs[0]
for i in range(1, len(wavs)):
    final_wav = np.concatenate([final_wav, silent_wav, wavs[i]])
final_wav = librosa.util.normalize(final_wav)

print("TIME: ", time.time() - start)
print(final_wav)
sampling_rate = 22050
import soundfile as sf

sf.write('demo.wav', final_wav, sampling_rate)
# ipd.display(ipd.Audio(audio, rate=hps.data.sampling_rate, normalize=False))